import {
    useEffect,
    useState
} from 'react';

export interface InitialValues {
    entry?: any;
    root?: any;
    rootMargin?: string;
    threshold?: number|[];
    callbacks?: any;
};

export interface IntersectionObserverEntry {
    boundingClientRect: any;
    intersectionRatio: any;
    intersectionRect: any;
    isIntersecting: any;
    rootBounds: any;
    target: any;
    time: any;
}

const compose = (...functions: any) => (args: any) => {
    return functions.reduce((arg: any, fn: any) => fn(arg), args)
};

export const useIntersectionObserver = (initalValues: InitialValues) => {
    
    const [currentValues, setValues] = useState(initalValues);

    useEffect(() => {
        const {
            callbacks = [],
            entry, 
            root = null,
            rootMargin = '0px' ,
            threshold = 0
        } = currentValues;
        
        const observer = new IntersectionObserver(
            ([entry]: any) => compose(...callbacks)(entry),
            {
                root,
                rootMargin,
                threshold
            }
        );
        if (entry && entry.current !== undefined) {
            observer.observe(entry.current);
        }
        return () => observer.disconnect();
    },[currentValues])

    return [
        currentValues,
        setValues
    ];
}

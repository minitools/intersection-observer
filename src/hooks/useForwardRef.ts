import {
	useEffect,
	useRef
} from 'react';

export const useForwardRef = (ref:any) => {
    const targetRef = useRef();
    useEffect(() => {
        if (!ref) return;
        if (typeof ref === 'function') {
          ref(targetRef.current)
        } else {
          ref.current = targetRef.current
        }
    }, [ref]);

    return targetRef
}


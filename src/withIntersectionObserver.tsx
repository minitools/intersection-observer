import React, {
    forwardRef,
    useLayoutEffect,
    useState
} from 'react';
import {useIntersectionObserver, IntersectionObserverEntry} from './hooks/useIntersectionObserver';
import {useForwardRef} from './hooks/useForwardRef';

export interface DefaultsForComponentType {
    observeOnlyUntilInScene: boolean;
    onInScene: Function;
    onOutScene: Function;
    placeholder?: Function;
    root?: any;
    rootMargin?: any;
    threshold?: number|[];
    wrapper?: Function;
}

const withIntersectionObserver = (WrappedComponent: any, defaultsForComponentType: DefaultsForComponentType) => {

    return forwardRef((props: any, ref: any) => {

        const forwardedRef = useForwardRef(ref);
        const {
            onInScene = defaultsForComponentType.onInScene,
            onOutScene = defaultsForComponentType.onOutScene,
            observeOnlyUntilInScene = defaultsForComponentType.observeOnlyUntilInScene,
            placeholder = defaultsForComponentType.placeholder,
            root = defaultsForComponentType.root,
            rootMargin = defaultsForComponentType.rootMargin,
            threshold = defaultsForComponentType.threshold,
            wrapper = defaultsForComponentType.wrapper,
            ...originalComponentProps
        } = props;
        
        const [currentProps, setCurrentProps] = useState(originalComponentProps);
        const [intersectionObserverEntry, setIntersectionObserverEntry] = useState<IntersectionObserverEntry| undefined>(undefined);

        const callback = (entry: IntersectionObserverEntry) => {
            setIntersectionObserverEntry(entry);
            if(observeOnlyUntilInScene && entry.intersectionRatio) {
                setIOValues({
                    ...IOValues,
                    // please note that an empty array as callbacks, will trigger observer.disconnect()
                    callbacks: []
                })
            }
        };

        const [IOValues, setIOValues]: any = useIntersectionObserver({
            entry: forwardedRef,
            root: root,
            rootMargin: rootMargin,
            threshold: threshold,
            callbacks: [callback]
        });

        useLayoutEffect(() => {
            setIOValues({
                ...IOValues,
                entry: forwardedRef
            })
        }, [
            forwardedRef, 
            intersectionObserverEntry
        ]);

        useLayoutEffect(() => {
            if (intersectionObserverEntry?.intersectionRatio) {
                onInScene(intersectionObserverEntry, originalComponentProps, setCurrentProps)
            } else {
                onOutScene(intersectionObserverEntry, originalComponentProps, setCurrentProps)
            }
        }, [intersectionObserverEntry]);
        
        if (!placeholder || intersectionObserverEntry?.intersectionRatio) {
            if (wrapper) {
                 return wrapper(WrappedComponent, currentProps, forwardedRef);
            }
            return <WrappedComponent forwardRef={forwardedRef} {...currentProps} />
        }
        else {
            return placeholder(forwardedRef)
        }
     });
};
export default withIntersectionObserver;

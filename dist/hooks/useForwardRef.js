import { useEffect, useRef } from 'react';
export var useForwardRef = function (ref) {
    var targetRef = useRef();
    useEffect(function () {
        if (!ref)
            return;
        if (typeof ref === 'function') {
            ref(targetRef.current);
        }
        else {
            ref.current = targetRef.current;
        }
    }, [ref]);
    return targetRef;
};

/// <reference types="react" />
export declare const useForwardRef: (ref: any) => import("react").MutableRefObject<undefined>;

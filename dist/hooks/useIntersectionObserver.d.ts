/// <reference types="react" />
export interface InitialValues {
    entry?: any;
    root?: any;
    rootMargin?: string;
    threshold?: number | [];
    callbacks?: any;
}
export interface IntersectionObserverEntry {
    boundingClientRect: any;
    intersectionRatio: any;
    intersectionRect: any;
    isIntersecting: any;
    rootBounds: any;
    target: any;
    time: any;
}
export declare const useIntersectionObserver: (initalValues: InitialValues) => (InitialValues | import("react").Dispatch<import("react").SetStateAction<InitialValues>>)[];

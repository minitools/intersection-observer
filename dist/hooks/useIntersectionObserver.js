import { useEffect, useState } from 'react';
;
var compose = function () {
    var functions = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        functions[_i] = arguments[_i];
    }
    return function (args) {
        return functions.reduce(function (arg, fn) { return fn(arg); }, args);
    };
};
export var useIntersectionObserver = function (initalValues) {
    var _a = useState(initalValues), currentValues = _a[0], setValues = _a[1];
    useEffect(function () {
        var _a = currentValues.callbacks, callbacks = _a === void 0 ? [] : _a, entry = currentValues.entry, _b = currentValues.root, root = _b === void 0 ? null : _b, _c = currentValues.rootMargin, rootMargin = _c === void 0 ? '0px' : _c, _d = currentValues.threshold, threshold = _d === void 0 ? 0 : _d;
        var observer = new IntersectionObserver(function (_a) {
            var entry = _a[0];
            return compose.apply(void 0, callbacks)(entry);
        }, {
            root: root,
            rootMargin: rootMargin,
            threshold: threshold
        });
        if (entry && entry.current !== undefined) {
            observer.observe(entry.current);
        }
        return function () { return observer.disconnect(); };
    }, [currentValues]);
    return [
        currentValues,
        setValues
    ];
};

import React from 'react';
export interface DefaultsForComponentType {
    observeOnlyUntilInScene: boolean;
    onInScene: Function;
    onOutScene: Function;
    placeholder?: Function;
    root?: any;
    rootMargin?: any;
    threshold?: number | [];
    wrapper?: Function;
}
declare const withIntersectionObserver: (WrappedComponent: any, defaultsForComponentType: DefaultsForComponentType) => React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & React.RefAttributes<unknown>>;
export default withIntersectionObserver;

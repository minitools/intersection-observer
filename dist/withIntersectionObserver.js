var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import React, { forwardRef, useLayoutEffect, useState } from 'react';
import { useIntersectionObserver } from './hooks/useIntersectionObserver';
import { useForwardRef } from './hooks/useForwardRef';
var withIntersectionObserver = function (WrappedComponent, defaultsForComponentType) {
    return forwardRef(function (props, ref) {
        var _a;
        var forwardedRef = useForwardRef(ref);
        var _b = props.onInScene, onInScene = _b === void 0 ? defaultsForComponentType.onInScene : _b, _c = props.onOutScene, onOutScene = _c === void 0 ? defaultsForComponentType.onOutScene : _c, _d = props.observeOnlyUntilInScene, observeOnlyUntilInScene = _d === void 0 ? defaultsForComponentType.observeOnlyUntilInScene : _d, _e = props.placeholder, placeholder = _e === void 0 ? defaultsForComponentType.placeholder : _e, _f = props.root, root = _f === void 0 ? defaultsForComponentType.root : _f, _g = props.rootMargin, rootMargin = _g === void 0 ? defaultsForComponentType.rootMargin : _g, _h = props.threshold, threshold = _h === void 0 ? defaultsForComponentType.threshold : _h, _j = props.wrapper, wrapper = _j === void 0 ? defaultsForComponentType.wrapper : _j, originalComponentProps = __rest(props, ["onInScene", "onOutScene", "observeOnlyUntilInScene", "placeholder", "root", "rootMargin", "threshold", "wrapper"]);
        var _k = useState(originalComponentProps), currentProps = _k[0], setCurrentProps = _k[1];
        var _l = useState(undefined), intersectionObserverEntry = _l[0], setIntersectionObserverEntry = _l[1];
        var callback = function (entry) {
            setIntersectionObserverEntry(entry);
            if (observeOnlyUntilInScene && entry.intersectionRatio) {
                setIOValues(__assign(__assign({}, IOValues), { 
                    // please note that an empty array as callbacks, will trigger observer.disconnect()
                    callbacks: [] }));
            }
        };
        var _m = useIntersectionObserver({
            entry: forwardedRef,
            root: root,
            rootMargin: rootMargin,
            threshold: threshold,
            callbacks: [callback]
        }), IOValues = _m[0], setIOValues = _m[1];
        useLayoutEffect(function () {
            setIOValues(__assign(__assign({}, IOValues), { entry: forwardedRef }));
        }, [
            forwardedRef,
            intersectionObserverEntry
        ]);
        useLayoutEffect(function () {
            var _a;
            if ((_a = intersectionObserverEntry) === null || _a === void 0 ? void 0 : _a.intersectionRatio) {
                onInScene(intersectionObserverEntry, originalComponentProps, setCurrentProps);
            }
            else {
                onOutScene(intersectionObserverEntry, originalComponentProps, setCurrentProps);
            }
        }, [intersectionObserverEntry]);
        if (!placeholder || ((_a = intersectionObserverEntry) === null || _a === void 0 ? void 0 : _a.intersectionRatio)) {
            if (wrapper) {
                return wrapper(WrappedComponent, currentProps, forwardedRef);
            }
            return React.createElement(WrappedComponent, __assign({ forwardRef: forwardedRef }, currentProps));
        }
        else {
            return placeholder(forwardedRef);
        }
    });
};
export default withIntersectionObserver;

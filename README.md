
##### Important

    This project is Work In Progress and is not available yet in npm


##### Install

    npm i intersection-observer-hoc

#### Usage case examples

Imagine you want provide your web application with a lazy loading system for images, iframes or 
any other resource. For the Images you could have a component like:

```js    
import 
    withIntersectionObserver, 
    {DefaultsForComponentType}  from 'intersection-observer-hoc'


export interface ImageInterface {
    alt: string;
    className?: string;
    forwardRef?: any;
    mediaSources?: any;
    src: string;
    title: string;
}

const Image: React.FC<ImageInterface> = (props) => {

    const {
        alt,
        className,
        mediaSources,
        src,
        title,
        forwardRef
    } = props;


        const sources = mediaSources
            ? mediaSources.map(
                ({media, source}: any) => <source key={media} media={media} srcSet={'blank'} data-srcset={source} ></source>
                )
            : null;

        return (
            <picture>
                {sources
                    ? sources
                    : null
                }
                <img
                    alt={alt}
                    className={className}
                    ref={forwardRef}
                    src={src}
                    title={title}
                />
            </picture>
        );
};

const defaults: DefaultsForComponentType = {
    observeOnlyUntilInScene: true,
    onInScene: (entry: any, props: any, setter: any) => {
        setter({...props})
    },
    onOutScene: (entry: any, props: any, setter: any) => null,
    placeholder: (ref: any) => <div ref={ref}></div>
};

const asyncImage = withIntersectionObserver(Image, defaults);
export {
     asyncImage as default
} ;

```

Please, pay attention in 2 things:
 
 - 1. the component use a prop called forwardRef which is provided by the HOC
 - 2. withIntersectionObserver take as second parameter an object called defaults.

 

 #### DefaultsForComponentType

| Property Name | Type | Required | Description |
|----------|:-------------:|:-------------:|------:|
| observeOnlyUntilInScene | Boolean | true | used for trigger observer.disconnect() |
| onInScene | function | true | callback triggered when target come in scene |
| onOutScene | function | true | callback triggered when target come out of scene |
| placeholder | function | false | if you didn't need render your component until is in scene, you can choice pass this function and return whatever you want instead |
| root | any | false | this property will be passed to the Intersection Observer |
| rootMargin | any | false | this property will be passed to the Intersection Observer |
| threshold | number or [] | false | this property will be passed to the Intersection Observer |
| wrapper | function | false | you can use a wrapper if your wrapped component dosen't use forwardRef |



